class TopController < ApplicationController
    def main
        if session[:login_uid] == nil then
            render 'login'
        else
            render 'main'
        end
        
    end
    
    def login
     if user = User.find_by_uid(params[:uid])
        uid = params[:uid]
        pass = params[:pass]
        if uid == user.uid && pass == user.pass then
            session[:login_uid] = :uid
            redirect_to '/'
        else
            render 'error'
        end
     else
            render 'error'
     end
    end
    
    def logout
        session.delete(:login_uid)
        redirect_to '/'
    end
end
